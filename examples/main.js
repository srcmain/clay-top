import Vue from 'vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'

import '@/styles/index.scss' // global css

import App from './App.vue'

Vue.use(ElementUI)

/* 全局注册 */

/* 统一加载: prod */
// import ClayTop from '@chaincloud/ClayTop'
// Vue.use(ClayTop)

/* 只加载Topology: prod */
// import { Topology } from '@chaincloud/ClayTop'
// Vue.use(Topology)

/* 统一加载: dev */
// import ClayTop from '../packages/index'
// Vue.use(ClayTop)

/* 只加载Topology: dev */
// import { Topology } from '../packages/index';
// Vue.use(Topology);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
