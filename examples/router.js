import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'DemoTopology',
      component: () =>
        import(/* webpackChunkName: "demo-topology" */ '@/views/demos/topology.vue')
    },
    {
      path: '*',
      component: () =>
        import(/* webpackChunkName: "home" */ '@/views/error-pages/404.vue')
    },
  ]
})

/**
 * 验证
 */
router.beforeEach((to, from, next) => {
  if (navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i)) {
    if (to.path === '/mobile') {
      next()
    } else {
      next('/mobile')
    }
  } else {
    if (to.path === '/mobile') {
      next('/home')
    } else {
      next()
    }
  }
})

export default router
