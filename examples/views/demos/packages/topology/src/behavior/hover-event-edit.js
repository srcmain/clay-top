/**
 * @author: clay
 * @data: 2019/07/16
 * @description: edit mode: 悬浮交互
 */
// 用来获取调用此js的vue组件实例（this）
let vm = null;
let hourItem = null;
const sendThis = (_this) => {
  vm = _this;
};
export default {
  sendThis, // 暴露函数
  name: "hover-event-edit",
  options: {
    getEvents() {
      return {
        "node:mouseover": "onNodeHover",
        "node:mouseout": "onNodeOut",
        "node:mouseleave":"onNodeLeave",
      };
    },
    onNodeHover(event) {
      let graph = vm.graph;
      let hoverNode = event.item;

      const name = event.shape.get("name");
      const item = event.item;

      if (name && name.startsWith("item")) {
        graph.updateItem(item, {
          selectedIndex: Number(name.split("-")[1])
        });
      } else {
        graph.updateItem(item, {
          selectedIndex: NaN
        });
      }
      if (name && name.startsWith("marker")) {
        hoverNode.setState("hover", true, graph);
        hourItem=hoverNode;
      }else {
        if (hourItem!=null){
          hourItem.setState("hover", false)
        }
      }
    },
    onNodeOut(event) {
      const name = event.shape.get("name");
      let hoverNode = event.item;
      if (name && name.startsWith("marker")) {
        hoverNode.setState("hover", false);
      }
      if (hourItem!=null){
        hourItem.setState("hover", false)
      }
      hoverNode.setState("hover", false);
    },
    onNodeLeave(event) {
      // if (hourItem!=null){
      //   hourItem.setState("hover", false)
      // }
    }
  }
};
