/**
 * @author: clay
 * @data: 2019/07/16
 * @description: register behaviors
 */

import dragAddEdge from './drag-add-edge'
import hoverEventEdit from './hover-event-edit'
import dragEventEdit from './drag-event-edit'
import keyupEventEdit from './keyup-event-edit'
import clickErNode from './click-er-node'
import clickErEdge from './click-er-edge'

const obj = {
  dragAddEdge,
  hoverEventEdit,
  dragEventEdit,
  keyupEventEdit,
  clickErNode,
  clickErEdge
}

export default {
  obj,
  register(G6) {
    Object.values(obj).map(item => {
      G6.registerBehavior(item.name, item.options)
    })
  }
}
