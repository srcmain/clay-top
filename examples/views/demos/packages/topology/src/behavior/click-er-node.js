/**
 * @author: clay
 * @data: 2021/5/14 23:20
 * @email: clay@hchyun.com
 * @description: node er图点击事件
 */
const isInBBox = (point, bbox) => {
  const {
    x,
    y
  } = point;
  const {
    minX,
    minY,
    maxX,
    maxY
  } = bbox;

  return x < maxX && x > minX && y > minY && y < maxY;
};

let vm = null;

const sendThis = (_this) => {
  vm = _this;
};

export default {
  sendThis,
  name: "click-er-node",
  options: {
    getEvents() {
      return {
        wheel: "scroll",
        "node:click": "onNodeClick",
        "node:contextmenu": "onNodeRightClick",
        "canvas:click": "onCanvasClick"
      };
    },
    //滚动事件监听
    scroll(event) {
      //禁止滚动的默认事件
      event.preventDefault();
      if (vm.clickCtrl) {
        if (event.deltaY>0){
          let graph = vm.graph
          if (graph && !graph.destroyed) {
            graph.zoom(0.8,{ x: event.x, y: event.y })
            //graph.zoom(0.8,{ x: event.clientX, y: event.clientY })
          }
        }else {
          let graph = vm.graph
          if (graph && !graph.destroyed) {
            graph.zoom(1.2,{ x: event.x, y: event.y })
            //graph.zoom(1.2,{ x: event.clientX, y: event.clientY })
          }
        }
      } else {
        const {
          graph
        } = this;
        const nodes = graph.getNodes().filter((n) => {
          const bbox = n.getBBox();
          return isInBBox(graph.getPointByClient(event.clientX, event.clientY), bbox);
        });
        if (nodes) {
          nodes.forEach((node) => {
            const model = node.getModel();
            if (model.columns.length < 9) {
              return;
            }
            const idx = model.startIndex || 0;
            this.start = idx;
            let startX = model.startX || 0.5;
            let startIndex = idx + event.deltaY * 0.02;
            if ((model.columns.length - idx) < 10 && startIndex > idx) {
              return;
            }
            startX -= event.deltaX;
            if (startIndex < 0) {
              startIndex = 0;
            }
            if (startX > 0) {
              startX = 0;
            }
            if (startIndex > model.columns.length - 1) {
              startIndex = model.columns.length - 1;
            }
            graph.update(node, {
              startIndex,
              startX
            });
          });
        }
      }
    },
    //节点左键点击监听
    onNodeClick(event) {
      this.shrinkage(event);
      this.updateVmData(event);
      vm.currentFocus = "node";
      vm.rightMenuShow = false;
    },
    /**
     * todo 右键打开设置面板
     * @param event
     */
    onNodeRightClick(event) {
      this.updateVmData(event);
      vm.tableColumnEditOpen = true;
    },
    onCanvasClick() {
      vm.currentFocus = "canvas";
      vm.rightMenuShow = false;
    },
    updateVmData(event) {
      let clickNode = event.item;
      clickNode.setState("selected", true);
      let clickNodeModel = clickNode.getModel();
      vm.selectedNode = clickNode;
      let nodeAppConfig = { ...vm.nodeAppConfig };
      Object.keys(nodeAppConfig).forEach(function(key) {
        nodeAppConfig[key] = "";
      });
      vm.selectedNodeParams = {
        label: clickNodeModel.label || "",
        columns: clickNodeModel.columns,
        appConfig: { ...nodeAppConfig, ...clickNodeModel.appConfig }
      };
      // }
    },
    shrinkage(e) {
      const {
        graph
      } = this;
      const item = e.item;
      const shape = e.shape;
      if (!item) {
        return;
      }
      if (shape.get("name") === "collapse") {
        graph.updateItem(item, {
          collapsed: true,
          size: [300, 50],
          height: 44
        });
        setTimeout(() => graph.layout(), 100);
      } else if (shape.get("name") === "expand") {
        graph.updateItem(item, {
          collapsed: false,
          size: [300, 500],
          height: 316
        });
        setTimeout(() => graph.layout(), 100);
      } else {
        const model = item.getModel();
      }
    },
  }
};
