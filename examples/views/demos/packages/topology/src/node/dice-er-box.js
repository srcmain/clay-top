/**
 * @author: clay
 * @data: 2021/5/15 0:16
 * @email: clay@hchyun.com
 * @description: draw anchor 绘制锚点
 */
import utils from '../utils/index'

const itemHeight = 30;
export default {
  name: 'dice-er-box',
  options: {
    setState(name, value, item) {
      // 设置节点状态
      utils.node.setState(name, value, item);
      // 设置锚点状态
      utils.anchor.setState(name, value, item);
    },
    draw(cfg, group) {
      const width = 250;
      const height = 316;
      const itemCount = 10;
      const boxStyle = {
        stroke: "#096DD9",
        radius: 4,
      };

      const {
        columns = [],
        startIndex = 0,
        selectedIndex,
        collapsed,
        icon,
      } = cfg;
      const list = columns;
      const afterList = list.slice(
        Math.floor(startIndex),
        Math.floor(startIndex + itemCount - 1)
      );
      const offsetY = (0.5 - (startIndex % 1)) * itemHeight + 30;

      //设置表名的容器
      group.addShape("rect", {
        attrs: {
          fill: boxStyle.stroke,
          height: 30,
          width,
          radius: [boxStyle.radius, boxStyle.radius, 0, 0],
        },
        draggable: true,
      });

      //设置左侧字体的边距
      let fontLeft = 12;

      // 设置图标
      if (icon && icon.show !== false) {
        group.addShape("image", {
          attrs: {
            x: 8,
            y: 8,
            height: 16,
            width: 16,
            ...icon,
          },
        });
        fontLeft += 18;
      }

      //设置表名
      group.addShape("text", {
        attrs: {
          y: 22,
          x: fontLeft,
          fill: "#fff",
          text: cfg.label,
          fontSize: 12,
          fontWeight: 500,
        },
      });

      //设置收缩部分的容器
      group.addShape("rect", {
        attrs: {
          x: 0,
          y: collapsed ? 30 : 300,
          height: 15,
          width,
          fill: "#eee",
          radius: [0, 0, boxStyle.radius, boxStyle.radius],
          cursor: "pointer",
        },
        name: collapsed ? "expand" : "collapse",
      });

      //设置收缩显示字符
      group.addShape("text", {
        attrs: {
          x: width / 2 - 6,
          y: (collapsed ? 30 : 300) + 12,
          text: collapsed ? "+" : "-",
          width,
          fill: "#000",
          radius: [0, 0, boxStyle.radius, boxStyle.radius],
          cursor: "pointer",
        },
        name: collapsed ? "expand" : "collapse",
      });

      //设置外边框
      const keyshape = group.addShape("rect", {
        attrs: {
          name: 'border',
          x: 0,
          y: 0,
          width,
          height: collapsed ? 45 : height,
          ...boxStyle,
        },
        //是否被允许拖拽
        draggable: true
      });

      //如果收缩状态,则返回当前图形
      if (collapsed) {
        return keyshape;
      }
      //添加空白组
      const listContainer = group.addGroup({});
      //todo 设置裁剪对象,字体加粗?
      listContainer.setClip({
        type: "rect",
        attrs: {
          x: -8,
          y: 30,
          width: width + 16,
          height: 300 - 30,
        },
      });
      listContainer.addShape({
        type: "rect",
        attrs: {
          x: 1,
          y: 30,
          width: width - 2,
          height: 300 - 30,
          fill: "#fff",
        },
        draggable: true,
      });

      //如果list中的column字段超过10个
      if (list.length > itemCount) {
        const barStyle = {
          width: 4,
          padding: 0,
          boxStyle: {
            stroke: "#00000022",
          },
          innerStyle: {
            fill: "#00000022",
          },
        };

        listContainer.addShape("rect", {
          attrs: {
            y: 30,
            x: width - barStyle.padding - barStyle.width,
            width: barStyle.width,
            height: height - 30,
            ...barStyle.boxStyle,
          },
        });

        //设置矩形高度
        const indexHeight =
          afterList.length > itemCount ?
            (afterList.length / list.length) * height :
            10;

        listContainer.addShape("rect", {
          attrs: {
            y: 30 +
              barStyle.padding +
              (startIndex / (list.length-8)) * (height - 30),
            x: width - barStyle.padding - barStyle.width,
            width: barStyle.width,
            height: Math.min(height, indexHeight),
            ...barStyle.innerStyle,
          },
        });
      }
      //渲染显示区域
      if (afterList) {
        afterList.forEach((e, i) => {
          //设置选中的列
          const isSelected = Math.floor(startIndex) + i === Number(selectedIndex);
          let {
            columnName = "", columnType,columnComment
          } = e;
          if (columnComment){
            columnName+= " : " + columnComment
          }
          if (columnType) {
            columnName += " - " + columnType;
          }
          const label = columnName.length > 26 ? columnName.slice(0, 24) + "..." : columnName;

          listContainer.addShape("rect", {
            attrs: {
              x: 1,
              y: i * itemHeight - itemHeight / 2 + offsetY,
              width: width - 4,
              height: itemHeight,
              radius: 2,
              fill:isSelected ? "#ddd" : "#fff",
              lineWidth: 1,
              cursor: "pointer",
            },
            name: `item-${Math.floor(startIndex) + i}-content`,
            draggable: true,
          });

          listContainer.addShape("text", {
            attrs: {
              x: 12,
              y: i * itemHeight + offsetY + 6,
              text: label,
              fontSize: 12,
              fill: "#000",
              fontFamily: "Avenir,-apple-system,BlinkMacSystemFont,Segoe UI,PingFang SC,Hiragino Sans GB,Microsoft YaHei,Helvetica Neue,Helvetica,Arial,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol",
              full: e,
              fontWeight: isSelected ? 500 : 100,
              cursor: "pointer",
            },
            name: `item-${Math.floor(startIndex) + i}`,
          });

          //未来设置字段之间有锚点
          // if (!cfg.hideDot) {
          //   utils.anchor.erDrawLeft(group, label, 0, i * itemHeight + offsetY)
          //   utils.anchor.erDrawLeft(group,label,width,i * itemHeight + offsetY)
          //   listContainer.addShape("marker", {
          //     attrs: {
          //       x: 0,
          //       y: i * itemHeight + offsetY,
          //       r: 3,
          //       stroke: boxStyle.stroke,
          //       fill: "white",
          //       radius: 2,
          //       lineWidth: 1,
          //       cursor: "crosshair",
          //     },
          //
          //     name: 'marker-shape'
          //   });
          //   listContainer.addShape("marker", {
          //     attrs: {
          //       x: width,
          //       y: i * itemHeight + offsetY,
          //       r: 3,
          //       stroke: boxStyle.stroke,
          //       fill: "white",
          //       radius: 2,
          //       lineWidth: 1,
          //       cursor: "crosshair",
          //
          //
          //     },
          //     name: 'marker-shape'
          //   });
          // }
        });
      }
      return keyshape;
    },
    // getAnchorPoints() {
    //   return [
    //     [0, 0],
    //     [1, 0],
    //   ];
    // },
    // 绘制后附加锚点
    afterDraw(cfg, group) {
      // 绘制锚点
      utils.anchor.drawMark(cfg, group)
    },

  }
}





