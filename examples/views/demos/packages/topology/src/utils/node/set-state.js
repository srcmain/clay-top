/**
 * @author: clay
 * @data: 2019/08/15
 * @description: set node state
 */

import theme from '../../theme'

export default function(name, value, item) {
  const group = item.getContainer()
  const themeStyle = theme.defaultStyle // todo...先使用默认主题，后期可能增加其它风格的主体
  let children = group.get('children')
  for (let i = 0; i < children.length; i++) {
    let child = children[i]
    //判断是否为er图的外围,是则改变样式
    if (child.attrs.name === 'border') {
      if (name === 'selected') {
        if (value) {
          child.attr(themeStyle.nodeStyle.selected)
        } else {
          child.attr(themeStyle.nodeStyle.default)
        }
      }
    }
  }
}
