/**
 * @author: clay
 * @data: 2019/08/15
 * @description: anchor
 */

import setState from './set-state'
import drawMark from './draw_mark'

export default {
  setState,
  drawMark,
}
