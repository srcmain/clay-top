/**
 * @author: clay
 * @data: 2021/5/11 17:28
 * @email: clay@hchyun.com
 * @description: node 节点收缩和放大
 */
export default function(e){
  const {
    graph
  } = this;
  const item = e.item;
  const shape = e.shape;
  if (!item) {
    return;
  }

  //收缩
  if (shape.get("name") === "collapse") {
    graph.updateItem(item, {
      collapsed: true,
      size: [300, 50],
    });
    setTimeout(() => graph.layout(), 100);
    //展开
  } else if (shape.get("name") === "expand") {
    graph.updateItem(item, {
      collapsed: false,
      size: [300, 500],
    });
    setTimeout(() => graph.layout(), 100);
  }
}
