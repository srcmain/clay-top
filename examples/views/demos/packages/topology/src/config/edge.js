/**
 * @author: clay
 * @data: 2019/08/16
 * @description: 线条的后期设置
 */

export default {
  type: 'top-cubic',
  style: {
    startArrow: false,
    endArrow: true
  }
}
